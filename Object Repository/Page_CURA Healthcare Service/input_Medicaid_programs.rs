<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Medicaid_programs</name>
   <tag></tag>
   <elementGuidId>e9ae2cbc-a598-4fc0-bc7d-cf291eede2e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#radio_program_medicaid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='radio_program_medicaid']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>500d7085-1625-4cf5-8933-fa21be97a0d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>d6923b1d-c057-4c8d-a30f-6f5abfff2157</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>programs</value>
      <webElementGuid>e4eb679f-d7d1-46b9-97e3-a8d24e0b3324</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>radio_program_medicaid</value>
      <webElementGuid>1e3fde15-17ae-45f0-bc4f-f262eedb9f9a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Medicaid</value>
      <webElementGuid>1ba250b5-f9f4-4eaf-bc15-91f74902dabf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;radio_program_medicaid&quot;)</value>
      <webElementGuid>eed631ce-6c57-4406-8f34-d1a1f14b020d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='radio_program_medicaid']</value>
      <webElementGuid>5474414c-116c-4942-93cd-5f49f45226a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[3]/div/label[2]/input</value>
      <webElementGuid>368e2e4f-9644-469d-824b-c1e6ed27c26a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label[2]/input</value>
      <webElementGuid>b0449e48-f096-44bd-932e-c91bce65b930</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'radio' and @name = 'programs' and @id = 'radio_program_medicaid']</value>
      <webElementGuid>3fef4b06-683b-4cfb-8750-b36d5717661b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
